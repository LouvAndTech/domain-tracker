# domain-tracker

Python script that list every url using a specific domain name on every TDP available.

# Install and use 

To use the script, just use PIP to download dependencies, thel clone the repo and run `python3 main.py` in a terminal

## Dependencies

```
 - dnspython
 - requests
 - progress
 - threading
 - queue
 ```
