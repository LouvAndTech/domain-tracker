import dns.resolver
import requests
import urllib.request
import os.path
from progress.bar import Bar
from queue import  *
from threading import Thread
from time import time

'''
Building the thread
'''
class CheckWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue
            url,bar = self.queue.get()
            try:
                test_url(url)
            finally:
                bar.update()
                self.queue.task_done()

class Bar():

    def __init__(self,max):
        self.statut = 0
        self.maxim = max

    def update(self):
        self.statut+=1
        self.afficher()
    
    def afficher(self):
        bar = "█"*int((((self.statut+1)/self.maxim)*20))+"-"*int(((20-int(((self.statut+1)/self.maxim)*20))))
        print(f'{round((self.statut/self.maxim)*100)}% | {bar} | {self.statut} / {self.maxim}', end = "\r")
     

'''
Fonction to get all the tld
@param Nothing 
@return list with all the tld
'''
def tdlGrab():
    #import a list with all the TLD
    return str(urllib.request.urlopen('https://data.iana.org/TLD/tlds-alpha-by-domain.txt').read()).split('\\n')[1:-1]


'''
Input the site to look for 
@param nothing
@return a str with the site to look
'''
def input_user():
    return(str(input("Name of the site to track : ")))

'''
Add the info to the file
@param 
'''
def add_to_file(url,head=True):
    with open(os.path.join('output/',url.split('//')[1].split('.')[0]), 'a') as file:
        # Append 'hello' at the end of file
        file.write(url+('' if head else ' - No header')+'\n')

'''
Test the url 
@param the url to check for 
@return Boolean for the site exist 
@return boolean for header or not 
'''
def test_url(site_to_check):
    try:
        #Start with a DNS request
        dns.resolver.query(site_to_check.split('://')[1])
        head = requests.head(site_to_check,timeout=10)
        #if the request get a response
        print("Found : %s                             "%(site_to_check))
        if (head):
            add_to_file(site_to_check)
            return(True,True)
        #if she get a response but no head
        else:
            add_to_file(site_to_check,False)
            return(True,False)
    except:
        return(False,False)

'''
build the list of ip to check 
@param list pf all the tld 
@param the input user 
@return a list of site to check
'''
def build_list(tld,name):
    lst_site = []
    for protocole in range(2):
        for i in range(len(tld)):
            lst_site.append(str(("http://" if protocole else "https://")+name+"."+tld[i]))
    return(lst_site)


'''
Main function
'''
def main():
    #get user input
    site = input_user()
    #Starting time 
    t = time()
    #Get tld
    lst_tld = tdlGrab()
    #build list of url 
    lst_url = build_list(lst_tld,site)
    #start queue
    queue = Queue()
    #start bar
    b=Bar(len(lst_url))
    #Create 8 workers
    for i in range(100):
        #Base on the template
        worker = CheckWorker(queue)
        worker.daemon=True
        worker.start()
    #Add each urtl to the Queue
    for url_to_check in lst_url:
        queue.put((url_to_check,b))

    #Add the main node so we can be lock in the queue until the end
    queue.join()
    #print the end time 
    print("\n --- DONE ---\nIt takes : %.6ss"%(time()-t))

if __name__ == '__main__':
    main()